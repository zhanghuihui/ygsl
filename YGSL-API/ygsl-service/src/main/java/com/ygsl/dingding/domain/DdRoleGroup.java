package com.ygsl.dingding.domain;

import com.ygsl.common.annotation.Excel;
import com.ygsl.common.core.domain.BaseEntity;
import lombok.Data;

/**
 * 钉钉角色组对象 dd_role_group
 * 
 * @author ygsl
 * @date 2021-12-20
 */
@Data
public class DdRoleGroup extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 角色组主键 */
    private Long groupId;

    /** 角色组名称 */
    @Excel(name = "角色组名称")
    private String name;

}
