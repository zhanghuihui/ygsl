package com.ygsl.dingding.service.impl;

import com.ygsl.dingding.domain.DdRoleGroup;
import com.ygsl.dingding.mapper.DdRoleGroupMapper;
import com.ygsl.dingding.service.IDdRoleGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 钉钉角色组Service业务层处理
 * 
 * @author ygsl
 * @date 2021-12-20
 */
@Service
public class DdRoleGroupServiceImpl implements IDdRoleGroupService
{
    @Autowired
    private DdRoleGroupMapper ddRoleGroupMapper;

    /**
     * 查询钉钉角色组
     * 
     * @param groupId 钉钉角色组主键
     * @return 钉钉角色组
     */
    @Override
    public DdRoleGroup selectDdRoleGroupByGroupId(Long groupId)
    {
        return ddRoleGroupMapper.selectDdRoleGroupByGroupId(groupId);
    }

    /**
     * 查询钉钉角色组列表
     * 
     * @param ddRoleGroup 钉钉角色组
     * @return 钉钉角色组
     */
    @Override
    public List<DdRoleGroup> selectDdRoleGroupList(DdRoleGroup ddRoleGroup)
    {
        return ddRoleGroupMapper.selectDdRoleGroupList(ddRoleGroup);
    }

    /**
     * 新增钉钉角色组
     * 
     * @param ddRoleGroup 钉钉角色组
     * @return 结果
     */
    @Override
    public int insertDdRoleGroup(DdRoleGroup ddRoleGroup)
    {
        return ddRoleGroupMapper.insertDdRoleGroup(ddRoleGroup);
    }

    /**
     * 修改钉钉角色组
     * 
     * @param ddRoleGroup 钉钉角色组
     * @return 结果
     */
    @Override
    public int updateDdRoleGroup(DdRoleGroup ddRoleGroup)
    {
        return ddRoleGroupMapper.updateDdRoleGroup(ddRoleGroup);
    }

    /**
     * 批量删除钉钉角色组
     * 
     * @param groupIds 需要删除的钉钉角色组主键
     * @return 结果
     */
    @Override
    public int deleteDdRoleGroupByGroupIds(Long[] groupIds)
    {
        return ddRoleGroupMapper.deleteDdRoleGroupByGroupIds(groupIds);
    }

    /**
     * 删除钉钉角色组信息
     * 
     * @param groupId 钉钉角色组主键
     * @return 结果
     */
    @Override
    public int deleteDdRoleGroupByGroupId(Long groupId)
    {
        return ddRoleGroupMapper.deleteDdRoleGroupByGroupId(groupId);
    }
}
