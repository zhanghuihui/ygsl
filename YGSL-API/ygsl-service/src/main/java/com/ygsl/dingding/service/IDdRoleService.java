package com.ygsl.dingding.service;

import com.ygsl.dingding.domain.DdRole;

import java.util.List;

/**
 * 钉钉角色Service接口
 * 
 * @author ygsl
 * @date 2021-12-20
 */
public interface IDdRoleService 
{
    /**
     * 查询钉钉角色
     * 
     * @param roleId 钉钉角色主键
     * @return 钉钉角色
     */
    public DdRole selectDdRoleByRoleId(Long roleId);

    /**
     * 查询钉钉角色列表
     * 
     * @param ddRole 钉钉角色
     * @return 钉钉角色集合
     */
    public List<DdRole> selectDdRoleList(DdRole ddRole);

    /**
     * 新增钉钉角色
     * 
     * @param ddRole 钉钉角色
     * @return 结果
     */
    public int insertDdRole(DdRole ddRole);

    /**
     * 修改钉钉角色
     * 
     * @param ddRole 钉钉角色
     * @return 结果
     */
    public int updateDdRole(DdRole ddRole);

    /**
     * 批量删除钉钉角色
     * 
     * @param roleIds 需要删除的钉钉角色主键集合
     * @return 结果
     */
    public int deleteDdRoleByRoleIds(Long[] roleIds);

    /**
     * 删除钉钉角色信息
     * 
     * @param roleId 钉钉角色主键
     * @return 结果
     */
    public int deleteDdRoleByRoleId(Long roleId);

    DdRole selectRoleInfoByRoleName( String groupName, String roleName);
}
