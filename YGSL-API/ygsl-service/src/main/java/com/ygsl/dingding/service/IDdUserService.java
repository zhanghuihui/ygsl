package com.ygsl.dingding.service;

import java.util.List;
import com.ygsl.dingding.domain.DdUser;

/**
 * 钉钉用户信息Service接口
 * 
 * @author ygsl
 * @date 2021-12-21
 */
public interface IDdUserService 
{
    /**
     * 查询钉钉用户信息
     * 
     * @param userId 钉钉用户信息主键
     * @return 钉钉用户信息
     */
    public DdUser selectDdUserByUserId(String userId);

    /**
     * 查询钉钉用户信息列表
     * 
     * @param ddUser 钉钉用户信息
     * @return 钉钉用户信息集合
     */
    public List<DdUser> selectDdUserList(DdUser ddUser);

    /**
     * 新增钉钉用户信息
     * 
     * @param ddUser 钉钉用户信息
     * @return 结果
     */
    public int insertDdUser(DdUser ddUser);

    /**
     * 修改钉钉用户信息
     * 
     * @param ddUser 钉钉用户信息
     * @return 结果
     */
    public int updateDdUser(DdUser ddUser);

    /**
     * 批量删除钉钉用户信息
     * 
     * @param userIds 需要删除的钉钉用户信息主键集合
     * @return 结果
     */
    public int deleteDdUserByUserIds(String[] userIds);

    /**
     * 删除钉钉用户信息信息
     * 
     * @param userId 钉钉用户信息主键
     * @return 结果
     */
    public int deleteDdUserByUserId(String userId);
}
