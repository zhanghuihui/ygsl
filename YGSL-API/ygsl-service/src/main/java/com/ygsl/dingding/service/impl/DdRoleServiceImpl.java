package com.ygsl.dingding.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ygsl.dingding.mapper.DdRoleMapper;
import com.ygsl.dingding.domain.DdRole;
import com.ygsl.dingding.service.IDdRoleService;

/**
 * 钉钉角色Service业务层处理
 * 
 * @author ygsl
 * @date 2021-12-20
 */
@Service
public class DdRoleServiceImpl implements IDdRoleService 
{
    @Autowired
    private DdRoleMapper ddRoleMapper;

    /**
     * 查询钉钉角色
     * 
     * @param roleId 钉钉角色主键
     * @return 钉钉角色
     */
    @Override
    public DdRole selectDdRoleByRoleId(Long roleId)
    {
        return ddRoleMapper.selectDdRoleByRoleId(roleId);
    }

    /**
     * 查询钉钉角色列表
     * 
     * @param ddRole 钉钉角色
     * @return 钉钉角色
     */
    @Override
    public List<DdRole> selectDdRoleList(DdRole ddRole)
    {
        return ddRoleMapper.selectDdRoleList(ddRole);
    }

    /**
     * 新增钉钉角色
     * 
     * @param ddRole 钉钉角色
     * @return 结果
     */
    @Override
    public int insertDdRole(DdRole ddRole)
    {
        return ddRoleMapper.insertDdRole(ddRole);
    }

    /**
     * 修改钉钉角色
     * 
     * @param ddRole 钉钉角色
     * @return 结果
     */
    @Override
    public int updateDdRole(DdRole ddRole)
    {
        return ddRoleMapper.updateDdRole(ddRole);
    }

    /**
     * 批量删除钉钉角色
     * 
     * @param roleIds 需要删除的钉钉角色主键
     * @return 结果
     */
    @Override
    public int deleteDdRoleByRoleIds(Long[] roleIds)
    {
        return ddRoleMapper.deleteDdRoleByRoleIds(roleIds);
    }

    /**
     * 删除钉钉角色信息
     * 
     * @param roleId 钉钉角色主键
     * @return 结果
     */
    @Override
    public int deleteDdRoleByRoleId(Long roleId)
    {
        return ddRoleMapper.deleteDdRoleByRoleId(roleId);
    }

    @Override
    public DdRole selectRoleInfoByRoleName( String groupName, String roleName) {
        return ddRoleMapper.selectRoleInfoByRoleName(groupName, roleName);
    }
}
