package com.ygsl.dingding.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ygsl.dingding.mapper.DdDeptMapper;
import com.ygsl.dingding.domain.DdDept;
import com.ygsl.dingding.service.IDdDeptService;

/**
 * 钉钉部门列Service业务层处理
 * 
 * @author ygsl
 * @date 2021-12-21
 */
@Service
public class DdDeptServiceImpl implements IDdDeptService 
{
    @Autowired
    private DdDeptMapper ddDeptMapper;

    /**
     * 查询钉钉部门列
     * 
     * @param deptId 钉钉部门列主键
     * @return 钉钉部门列
     */
    @Override
    public DdDept selectDdDeptByDeptId(Long deptId)
    {
        return ddDeptMapper.selectDdDeptByDeptId(deptId);
    }

    /**
     * 查询钉钉部门列列表
     * 
     * @param ddDept 钉钉部门列
     * @return 钉钉部门列
     */
    @Override
    public List<DdDept> selectDdDeptList(DdDept ddDept)
    {
        return ddDeptMapper.selectDdDeptList(ddDept);
    }

    /**
     * 新增钉钉部门列
     * 
     * @param ddDept 钉钉部门列
     * @return 结果
     */
    @Override
    public int insertDdDept(DdDept ddDept)
    {
        return ddDeptMapper.insertDdDept(ddDept);
    }

    /**
     * 修改钉钉部门列
     * 
     * @param ddDept 钉钉部门列
     * @return 结果
     */
    @Override
    public int updateDdDept(DdDept ddDept)
    {
        return ddDeptMapper.updateDdDept(ddDept);
    }

    /**
     * 批量删除钉钉部门列
     * 
     * @param deptIds 需要删除的钉钉部门列主键
     * @return 结果
     */
    @Override
    public int deleteDdDeptByDeptIds(Long[] deptIds)
    {
        return ddDeptMapper.deleteDdDeptByDeptIds(deptIds);
    }

    /**
     * 删除钉钉部门列信息
     * 
     * @param deptId 钉钉部门列主键
     * @return 结果
     */
    @Override
    public int deleteDdDeptByDeptId(Long deptId)
    {
        return ddDeptMapper.deleteDdDeptByDeptId(deptId);
    }
}
