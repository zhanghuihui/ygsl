package com.ygsl.dingding.service.impl;

import java.util.List;
import com.ygsl.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ygsl.dingding.mapper.DdStartApproveMapper;
import com.ygsl.dingding.domain.DdStartApprove;
import com.ygsl.dingding.service.IDdStartApproveService;

/**
 * 钉钉发起审批信息Service业务层处理
 * 
 * @author ygsl
 * @date 2021-12-23
 */
@Service
public class DdStartApproveServiceImpl implements IDdStartApproveService 
{
    @Autowired
    private DdStartApproveMapper ddStartApproveMapper;

    /**
     * 查询钉钉发起审批信息
     * 
     * @param id 钉钉发起审批信息主键
     * @return 钉钉发起审批信息
     */
    @Override
    public DdStartApprove selectDdStartApproveById(Long id)
    {
        return ddStartApproveMapper.selectDdStartApproveById(id);
    }

    /**
     * 查询钉钉发起审批信息列表
     * 
     * @param ddStartApprove 钉钉发起审批信息
     * @return 钉钉发起审批信息
     */
    @Override
    public List<DdStartApprove> selectDdStartApproveList(DdStartApprove ddStartApprove)
    {
        return ddStartApproveMapper.selectDdStartApproveList(ddStartApprove);
    }

    /**
     * 新增钉钉发起审批信息
     * 
     * @param ddStartApprove 钉钉发起审批信息
     * @return 结果
     */
    @Override
    public int insertDdStartApprove(DdStartApprove ddStartApprove)
    {
        ddStartApprove.setCreateTime(DateUtils.getNowDate());
        return ddStartApproveMapper.insertDdStartApprove(ddStartApprove);
    }

    /**
     * 修改钉钉发起审批信息
     * 
     * @param ddStartApprove 钉钉发起审批信息
     * @return 结果
     */
    @Override
    public int updateDdStartApprove(DdStartApprove ddStartApprove)
    {
        return ddStartApproveMapper.updateDdStartApprove(ddStartApprove);
    }

    /**
     * 批量删除钉钉发起审批信息
     * 
     * @param ids 需要删除的钉钉发起审批信息主键
     * @return 结果
     */
    @Override
    public int deleteDdStartApproveByIds(Long[] ids)
    {
        return ddStartApproveMapper.deleteDdStartApproveByIds(ids);
    }

    /**
     * 删除钉钉发起审批信息信息
     * 
     * @param id 钉钉发起审批信息主键
     * @return 结果
     */
    @Override
    public int deleteDdStartApproveById(Long id)
    {
        return ddStartApproveMapper.deleteDdStartApproveById(id);
    }
}
