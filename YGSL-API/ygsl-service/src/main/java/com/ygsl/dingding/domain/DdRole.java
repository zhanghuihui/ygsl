package com.ygsl.dingding.domain;

import com.ygsl.common.annotation.Excel;
import com.ygsl.common.core.domain.BaseEntity;
import lombok.Data;

/**
 * 钉钉角色对象 dd_role
 * 
 * @author ygsl
 * @date 2021-12-20
 */
@Data
public class DdRole extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 角色主键 */
    private Long roleId;

    /** 角色名称 */
    @Excel(name = "角色名称")
    private String name;

    /** 角色组主键 */
    @Excel(name = "角色组主键")
    private Long groupId;
}
