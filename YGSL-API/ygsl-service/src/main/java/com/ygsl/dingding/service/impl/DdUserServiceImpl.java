package com.ygsl.dingding.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ygsl.dingding.mapper.DdUserMapper;
import com.ygsl.dingding.domain.DdUser;
import com.ygsl.dingding.service.IDdUserService;

/**
 * 钉钉用户信息Service业务层处理
 * 
 * @author ygsl
 * @date 2021-12-21
 */
@Service
public class DdUserServiceImpl implements IDdUserService 
{
    @Autowired
    private DdUserMapper ddUserMapper;

    /**
     * 查询钉钉用户信息
     * 
     * @param userId 钉钉用户信息主键
     * @return 钉钉用户信息
     */
    @Override
    public DdUser selectDdUserByUserId(String userId)
    {
        return ddUserMapper.selectDdUserByUserId(userId);
    }

    /**
     * 查询钉钉用户信息列表
     * 
     * @param ddUser 钉钉用户信息
     * @return 钉钉用户信息
     */
    @Override
    public List<DdUser> selectDdUserList(DdUser ddUser)
    {
        return ddUserMapper.selectDdUserList(ddUser);
    }

    /**
     * 新增钉钉用户信息
     * 
     * @param ddUser 钉钉用户信息
     * @return 结果
     */
    @Override
    public int insertDdUser(DdUser ddUser)
    {
        return ddUserMapper.insertDdUser(ddUser);
    }

    /**
     * 修改钉钉用户信息
     * 
     * @param ddUser 钉钉用户信息
     * @return 结果
     */
    @Override
    public int updateDdUser(DdUser ddUser)
    {
        return ddUserMapper.updateDdUser(ddUser);
    }

    /**
     * 批量删除钉钉用户信息
     * 
     * @param userIds 需要删除的钉钉用户信息主键
     * @return 结果
     */
    @Override
    public int deleteDdUserByUserIds(String[] userIds)
    {
        return ddUserMapper.deleteDdUserByUserIds(userIds);
    }

    /**
     * 删除钉钉用户信息信息
     * 
     * @param userId 钉钉用户信息主键
     * @return 结果
     */
    @Override
    public int deleteDdUserByUserId(String userId)
    {
        return ddUserMapper.deleteDdUserByUserId(userId);
    }
}
