package com.ygsl.dingding.mapper;

import com.ygsl.dingding.domain.DdRoleGroup;

import java.util.List;

/**
 * 钉钉角色组Mapper接口
 * 
 * @author ygsl
 * @date 2021-12-20
 */
public interface DdRoleGroupMapper 
{
    /**
     * 查询钉钉角色组
     * 
     * @param groupId 钉钉角色组主键
     * @return 钉钉角色组
     */
    public DdRoleGroup selectDdRoleGroupByGroupId(Long groupId);

    /**
     * 查询钉钉角色组列表
     * 
     * @param ddRoleGroup 钉钉角色组
     * @return 钉钉角色组集合
     */
    public List<DdRoleGroup> selectDdRoleGroupList(DdRoleGroup ddRoleGroup);

    /**
     * 新增钉钉角色组
     * 
     * @param ddRoleGroup 钉钉角色组
     * @return 结果
     */
    public int insertDdRoleGroup(DdRoleGroup ddRoleGroup);

    /**
     * 修改钉钉角色组
     * 
     * @param ddRoleGroup 钉钉角色组
     * @return 结果
     */
    public int updateDdRoleGroup(DdRoleGroup ddRoleGroup);

    /**
     * 删除钉钉角色组
     * 
     * @param groupId 钉钉角色组主键
     * @return 结果
     */
    public int deleteDdRoleGroupByGroupId(Long groupId);

    /**
     * 批量删除钉钉角色组
     * 
     * @param groupIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteDdRoleGroupByGroupIds(Long[] groupIds);
}
