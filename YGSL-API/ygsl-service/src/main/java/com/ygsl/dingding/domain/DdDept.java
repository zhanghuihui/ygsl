package com.ygsl.dingding.domain;

import com.ygsl.common.annotation.Excel;
import com.ygsl.common.core.domain.BaseEntity;
import lombok.Data;

/**
 * 钉钉部门列对象 dd_dept
 * 
 * @author ygsl
 * @date 2021-12-21
 */
@Data
public class DdDept extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 部门ID */
    private Long deptId;

    /** 父节点 */
    @Excel(name = "父节点")
    private Long parentId;

    /** 部门名称 */
    @Excel(name = "部门名称")
    private String name;

    /** 部门群已经创建后，有新人加入部门是否会自动加入该群;1-true; 0-false;  */
    private String createDeptGroup;

    /** $column.columnComment */
    private String autoAddUser;

    /** 是否默认同意加入该部门的申请;1-true; 0-false */
    private String autoApproveApply;

    /** 部门是否来自关联组织;1-true; 0-false;  */
    private String fromUnionOrg;

    /** 教育部门标签 */
    @Excel(name = "教育部门标签")
    private String tags;

    /** $column.columnComment */
    private Long deptOrder;

    /** 部门群ID */
    @Excel(name = "部门群ID")
    private String deptGroupChatId;

    /** 企业群群主userId */
    @Excel(name = "企业群群主userId")
    private String orgDeptOwner;

    /** 部门的主管userd列表 */
    @Excel(name = "部门的主管userd列表")
    private String deptManagerUseridList;

    /** 是否限制本部门成员查看通讯录;1-true; 0-false;  */
    private String outerDept;

    /** 当限制部门成员的通讯录查看范围时; */
    private String outerPermitDepts;

    /** 当限制部门成员的通讯录查看范围时（即outer_dept为true时），配置的部门员工可见员工列表 */
    private String outerPermitUsers;

    /** 是否隐藏本部门;1-true; 0-false;  */
    private String hideDept;

    /** 当隐藏本部门时（即hide_dept为true时），配置的允许在通讯录中查看本部门的员工列表 */
    private String userPermits;

    /** 当隐藏本部门时（即hide_dept为true时），配置的允许在通讯录中查看本部门的部门列表 */
    private String deptPermits;

    /** 部门标识字段 */
    private String sourceIdentifier;

    /** 部门群是否包含子部门;1-true; 0-false;  */
    private String groupContainSubDept;
}
