package com.ygsl.dingding.domain;

import com.ygsl.common.annotation.Excel;
import com.ygsl.common.core.domain.BaseEntity;
import lombok.Data;

/**
 * 钉钉用户信息对象 dd_user
 * 
 * @author ygsl
 * @date 2021-12-21
 */
@Data
public class DdUser extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 账号 */
    @Excel(name = "账号")
    private String userId;

    /** 用户名 */
    @Excel(name = "用户名")
    private String name;

    /** 员工在部门中的排序 */
    @Excel(name = "员工在部门中的排序")
    private String deptOrder;

    /** 用户在当前开发者企业账号范围内的唯一标识 */
    @Excel(name = "用户在当前开发者企业账号范围内的唯一标识")
    private String unionId;

    /** 头像地址 */
    @Excel(name = "头像地址")
    private String avatar;

    /** 国际电话区号 */
    @Excel(name = "国际电话区号")
    private String stateCode;

    /** 手机号码 */
    @Excel(name = "手机号码")
    private String mobile;

    /** 是否号码隐藏; 0-false; 1-true */
    @Excel(name = "是否号码隐藏; 0-false; 1-true")
    private String hideMobile;

    /** 分机号 */
    @Excel(name = "分机号")
    private String telephone;

    /** 员工工号 */
    @Excel(name = "员工工号")
    private String jobNumber;

    private String title;

    /** 员工邮箱 */
    @Excel(name = "员工邮箱")
    private String email;

    /** 员工的企业邮箱 */
    @Excel(name = "员工的企业邮箱")
    private String orgEmail;

    /** 办公地点 */
    @Excel(name = "办公地点")
    private String workPlace;

    /** 所属部门ID列表 */
    @Excel(name = "所属部门ID列表")
    private String deptIdList;

    /** 扩展属性 */
    @Excel(name = "扩展属性")
    private String extension;

    /** 入职时间 */
    @Excel(name = "入职时间")
    private Long hiredDate;

    /** 是否激活了钉钉; 0-false;1-true */
    @Excel(name = "是否激活了钉钉; 0-false;1-true")
    private String active;

    /** 是否为企业的管理员;0-false;1-true */
    @Excel(name = "是否为企业的管理员;0-false;1-true")
    private String admin;

    /** 是否为企业的老板;0-false;1-true */
    @Excel(name = "是否为企业的老板;0-false;1-true")
    private String boss;

    /** 是否是部门的主管;0-false;1-true */
    @Excel(name = "是否是部门的主管;0-false;1-true")
    private String leader;

    /** 是否专属帐号;0-false;1-true */
    @Excel(name = "是否专属帐号;0-false;1-true")
    private String exclusiveAccount;

    /** 专属帐号登录名 */
    @Excel(name = "专属帐号登录名")
    private String loginId;

    /** 专属帐号类型 */
    @Excel(name = "专属帐号类型")
    private String exclusiveAccountType;

    /** 查询本组织专属帐号时可获得昵称 */
    @Excel(name = "查询本组织专属帐号时可获得昵称")
    private String nickName;
}
