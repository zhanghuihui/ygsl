package com.ygsl.dingding.service;

import java.util.List;
import com.ygsl.dingding.domain.DdDept;

/**
 * 钉钉部门列Service接口
 * 
 * @author ygsl
 * @date 2021-12-21
 */
public interface IDdDeptService 
{
    /**
     * 查询钉钉部门列
     * 
     * @param deptId 钉钉部门列主键
     * @return 钉钉部门列
     */
    public DdDept selectDdDeptByDeptId(Long deptId);

    /**
     * 查询钉钉部门列列表
     * 
     * @param ddDept 钉钉部门列
     * @return 钉钉部门列集合
     */
    public List<DdDept> selectDdDeptList(DdDept ddDept);

    /**
     * 新增钉钉部门列
     * 
     * @param ddDept 钉钉部门列
     * @return 结果
     */
    public int insertDdDept(DdDept ddDept);

    /**
     * 修改钉钉部门列
     * 
     * @param ddDept 钉钉部门列
     * @return 结果
     */
    public int updateDdDept(DdDept ddDept);

    /**
     * 批量删除钉钉部门列
     * 
     * @param deptIds 需要删除的钉钉部门列主键集合
     * @return 结果
     */
    public int deleteDdDeptByDeptIds(Long[] deptIds);

    /**
     * 删除钉钉部门列信息
     * 
     * @param deptId 钉钉部门列主键
     * @return 结果
     */
    public int deleteDdDeptByDeptId(Long deptId);
}
