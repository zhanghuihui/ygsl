package com.ygsl.dingding.service;

import java.util.List;
import com.ygsl.dingding.domain.DdStartApprove;

/**
 * 钉钉发起审批信息Service接口
 * 
 * @author ygsl
 * @date 2021-12-23
 */
public interface IDdStartApproveService 
{
    /**
     * 查询钉钉发起审批信息
     * 
     * @param id 钉钉发起审批信息主键
     * @return 钉钉发起审批信息
     */
    public DdStartApprove selectDdStartApproveById(Long id);

    /**
     * 查询钉钉发起审批信息列表
     * 
     * @param ddStartApprove 钉钉发起审批信息
     * @return 钉钉发起审批信息集合
     */
    public List<DdStartApprove> selectDdStartApproveList(DdStartApprove ddStartApprove);

    /**
     * 新增钉钉发起审批信息
     * 
     * @param ddStartApprove 钉钉发起审批信息
     * @return 结果
     */
    public int insertDdStartApprove(DdStartApprove ddStartApprove);

    /**
     * 修改钉钉发起审批信息
     * 
     * @param ddStartApprove 钉钉发起审批信息
     * @return 结果
     */
    public int updateDdStartApprove(DdStartApprove ddStartApprove);

    /**
     * 批量删除钉钉发起审批信息
     * 
     * @param ids 需要删除的钉钉发起审批信息主键集合
     * @return 结果
     */
    public int deleteDdStartApproveByIds(Long[] ids);

    /**
     * 删除钉钉发起审批信息信息
     * 
     * @param id 钉钉发起审批信息主键
     * @return 结果
     */
    public int deleteDdStartApproveById(Long id);
}
