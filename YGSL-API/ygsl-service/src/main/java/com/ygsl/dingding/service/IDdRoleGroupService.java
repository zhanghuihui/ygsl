package com.ygsl.dingding.service;

import com.ygsl.dingding.domain.DdRoleGroup;

import java.util.List;

/**
 * 钉钉角色组Service接口
 * 
 * @author ygsl
 * @date 2021-12-20
 */
public interface IDdRoleGroupService 
{
    /**
     * 查询钉钉角色组
     * 
     * @param groupId 钉钉角色组主键
     * @return 钉钉角色组
     */
    public DdRoleGroup selectDdRoleGroupByGroupId(Long groupId);

    /**
     * 查询钉钉角色组列表
     * 
     * @param ddRoleGroup 钉钉角色组
     * @return 钉钉角色组集合
     */
    public List<DdRoleGroup> selectDdRoleGroupList(DdRoleGroup ddRoleGroup);

    /**
     * 新增钉钉角色组
     * 
     * @param ddRoleGroup 钉钉角色组
     * @return 结果
     */
    public int insertDdRoleGroup(DdRoleGroup ddRoleGroup);

    /**
     * 修改钉钉角色组
     * 
     * @param ddRoleGroup 钉钉角色组
     * @return 结果
     */
    public int updateDdRoleGroup(DdRoleGroup ddRoleGroup);

    /**
     * 批量删除钉钉角色组
     * 
     * @param groupIds 需要删除的钉钉角色组主键集合
     * @return 结果
     */
    public int deleteDdRoleGroupByGroupIds(Long[] groupIds);

    /**
     * 删除钉钉角色组信息
     * 
     * @param groupId 钉钉角色组主键
     * @return 结果
     */
    public int deleteDdRoleGroupByGroupId(Long groupId);
}
