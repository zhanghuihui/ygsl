package com.ygsl.dingding.mapper;

import com.ygsl.dingding.domain.DdRole;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 钉钉角色Mapper接口
 * 
 * @author ygsl
 * @date 2021-12-20
 */
public interface DdRoleMapper 
{
    /**
     * 查询钉钉角色
     * 
     * @param roleId 钉钉角色主键
     * @return 钉钉角色
     */
    public DdRole selectDdRoleByRoleId(Long roleId);

    /**
     * 查询钉钉角色列表
     * 
     * @param ddRole 钉钉角色
     * @return 钉钉角色集合
     */
    public List<DdRole> selectDdRoleList(DdRole ddRole);

    /**
     * 新增钉钉角色
     * 
     * @param ddRole 钉钉角色
     * @return 结果
     */
    public int insertDdRole(DdRole ddRole);

    /**
     * 修改钉钉角色
     * 
     * @param ddRole 钉钉角色
     * @return 结果
     */
    public int updateDdRole(DdRole ddRole);

    /**
     * 删除钉钉角色
     * 
     * @param roleId 钉钉角色主键
     * @return 结果
     */
    public int deleteDdRoleByRoleId(Long roleId);

    /**
     * 批量删除钉钉角色
     * 
     * @param roleIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteDdRoleByRoleIds(Long[] roleIds);

    DdRole selectRoleInfoByRoleName(@Param("groupName") String groupName, @Param("roleName")String roleName);
}
