package com.ygsl.dingding.domain;

import com.ygsl.common.annotation.Excel;
import com.ygsl.common.core.domain.BaseEntity;
import lombok.Data;

/**
 * 钉钉发起审批信息对象 dd_start_approve
 * 
 * @author ygsl
 * @date 2021-12-23
 */
@Data
public class DdStartApprove extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键，使用序列DD_START_APPROVE_SEQ */
    private Long id;

    /** 内部应用id */
    @Excel(name = "内部应用id")
    private Long agentId;

    /** 审批流唯一码 */
    @Excel(name = "审批流唯一码")
    private String processCode;

    /** 审批实例发起人 */
    @Excel(name = "审批实例发起人")
    private String originatorUserId;

    /** 部门id */
    @Excel(name = "部门id")
    private Long deptId;

    /** 审批人 */
    @Excel(name = "审批人")
    private String approvers;

    /** 设置审批人，会签、或签 */
    @Excel(name = "设置审批人，会签、或签")
    private String approversV2;

    /** 抄送人 */
    @Excel(name = "抄送人")
    private String ccList;

    /** 抄送节点 */
    @Excel(name = "抄送节点")
    private String ccPosition;

    /** 组件 */
    @Excel(name = "组件")
    private String formComponentValues;
}
