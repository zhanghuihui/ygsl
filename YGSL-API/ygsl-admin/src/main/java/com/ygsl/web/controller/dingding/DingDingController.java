package com.ygsl.web.controller.dingding;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.dingtalk.api.DefaultDingTalkClient;
import com.dingtalk.api.DingTalkClient;
import com.dingtalk.api.request.OapiProcessinstanceCreateRequest;
import com.dingtalk.api.request.OapiRoleSimplelistRequest;
import com.dingtalk.api.response.OapiProcessinstanceCreateResponse;
import com.dingtalk.api.response.OapiRoleSimplelistResponse;
import com.taobao.api.ApiException;
import com.ygsl.common.core.domain.AjaxResult;
import com.ygsl.common.utils.StringUtils;
import com.ygsl.dingding.domain.DdRole;
import com.ygsl.dingding.domain.DdStartApprove;
import com.ygsl.dingding.service.IDdRoleService;
import com.ygsl.dingding.service.IDdStartApproveService;
import com.ygsl.dingding.crypto.DingCallbackCrypto;
import com.ygsl.dingding.tools.Ddtools;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * 事件订阅
 * 
 * @author ygsl
 */
@RestController
@RequestMapping("/dingding")
public class DingDingController
{
    private static final Logger bizLogger = LoggerFactory.getLogger(DingDingController.class);
    @Value("${taobao.dingding.appKey}")
    private String appKey;

    @Value("${taobao.dingding.appSecret}")
    private String appSecret;

    @Value("${taobao.dingding.aesKey}")
    private String aesKey;

    @Value("${taobao.dingding.token}")
    private String token;
    /**
     * 应用标识
     */
    @Value("${taobao.dingding.agentId}")
    private Long agentId;
    @Value("${taobao.dingding.processCode}")
    private String processCode;

    @Autowired(required = false)
    private IDdRoleService ddRoleService;

    @Autowired(required = false)
    private IDdStartApproveService ddStartApproveService;
    /**
     *事件订阅
     * @param msg_signature 为消息体签名
     * @param timeStamp 为时间戳
     * @param nonce 为随机字符串
     * @param json 为加密的推送事件信息
     * @return
     */
    @PostMapping("/getcCallBack")
    public  Map<String, String> getcCallBack(@RequestParam(value = "msg_signature", required = false) String msg_signature,
                                   @RequestParam(value = "timestamp", required = false) String timeStamp,
                                   @RequestParam(value = "nonce", required = false) String nonce,
                                   @RequestBody(required = false) JSONObject json)
    {
        try {
            // 1. 从http请求中获取加解密参数
            // 2. 使用加解密类型
            // Constant.OWNER_KEY 说明：
            // 1、开发者后台配置的订阅事件为应用级事件推送，此时OWNER_KEY为应用的APP_KEY。
            // 2、调用订阅事件接口订阅的事件为企业级事件推送，
            // 此时OWNER_KEY为：企业的appkey（企业内部应用）或SUITE_KEY（三方应用）
            DingCallbackCrypto callbackCrypto = new DingCallbackCrypto(token, aesKey,appKey);
            String encryptMsg = json.getString("encrypt");
            String decryptMsg = callbackCrypto.getDecryptMsg(msg_signature, timeStamp, nonce, encryptMsg);

            // 3. 反序列化回调事件json数据
            JSONObject eventJson = JSON.parseObject(decryptMsg);
            String eventType = eventJson.getString("EventType");

            // 4. 根据EventType分类处理
            if ("check_url".equals(eventType)) {
                // 测试回调url的正确性
                bizLogger.info("测试回调url的正确性");
            } else if ("bpms_task_change".equals(eventType)) {
                //审批任务开始，结束，转交。
                bizLogger.info("发生了：" + eventType + "事件");
            } else if("bpms_instance_change".equals(eventType)){
                // 审批实例开始，结束。
                bizLogger.info("发生了：" + eventType + "事件");
            }
            // 5. 返回success的加密数据
            Map<String, String> successMap = callbackCrypto.getEncryptedMap("success");
            return successMap;

        } catch (DingCallbackCrypto.DingTalkEncryptException e) {
            e.printStackTrace();
        }
        return null;
    }
    /**
     * 根据角色组名称，角色名称 获取角色信息
     * @param groupName， roleName
     */
    @PostMapping("/getRoleInfoByRoleName")
    public AjaxResult getRoleInfoByRoleName(@RequestParam("groupName") String groupName, @RequestParam("roleName") String roleName){
        DdRole  ddRole = ddRoleService.selectRoleInfoByRoleName(groupName, roleName);
        if(StringUtils.isNotNull(ddRole)){
            return AjaxResult.success(ddRole);
        }else {
            return AjaxResult.error();
        }
    }
    /**
     * 根据角色id获取人员列表
     * @param roleId
     */
    @PostMapping("/getUserListByRoleId/{roleId}")
    public AjaxResult getUserListByRoleId(@PathVariable("roleId") Long roleId){
        String accessToken =  Ddtools.getAccessToken(appKey, appSecret);

        DingTalkClient client = new DefaultDingTalkClient("https://oapi.dingtalk.com/topapi/role/simplelist");
        OapiRoleSimplelistRequest req = new OapiRoleSimplelistRequest();
        req.setRoleId(roleId);
        OapiRoleSimplelistResponse rsp = null;
        try {
            rsp = client.execute(req, accessToken);
            bizLogger.info("获取钉钉角色下的人员列表:{}", rsp.getBody());
        } catch (ApiException e) {
            e.printStackTrace();
            bizLogger.info("获取钉钉角色下的人员列表失败;原因:{}", e.getErrMsg());
            return AjaxResult.error("获取钉钉角色下的人员列表失败");
        }
       return AjaxResult.success(JSONObject.parseObject(rsp.getBody()));
    }

    /**
     * 发起审批
     */
    @PostMapping("/startApproving")
    public AjaxResult startApproving(@RequestBody Map<String, Object> params){
        DdStartApprove startApprove = new DdStartApprove();
        String accessToken = Ddtools.getAccessToken(appKey, appSecret);
        DingTalkClient client = new DefaultDingTalkClient("https://oapi.dingtalk.com/topapi/processinstance/create");
        OapiProcessinstanceCreateRequest req = new OapiProcessinstanceCreateRequest();

        req.setAgentId(agentId);
        startApprove.setAgentId(agentId);

        req.setProcessCode(processCode);
        startApprove.setProcessCode(processCode);
        if(StringUtils.isNotNull(params.get("originator_user_id"))){
            String originator_user_id = params.get("originator_user_id").toString();
            //审批实例发起人的userid。
            req.setOriginatorUserId(originator_user_id);
            startApprove.setOriginatorUserId(originator_user_id);
        }
        if(StringUtils.isNotNull(params.get("dept_id"))){
            Long dept_id = Long.valueOf(params.get("dept_id").toString());
            req.setDeptId(dept_id);
            startApprove.setDeptId(dept_id);
        }
       //审批人userid列表，最大列表长度20。//多个审批人用逗号分隔，按传入的顺序依次审批。
        if(StringUtils.isNotNull(params.get("approvers"))){
            String approvers = params.get("approvers").toString();
            req.setApprovers(approvers);
            startApprove.setApprovers(approvers);
        }
        if(StringUtils.isNotNull(params.get("cc_list"))){
            String cc_list = params.get("cc_list").toString();
            //抄送人userid列表，最大列表长度20
            req.setCcList(cc_list);
            startApprove.setCcList(cc_list);
        }
        if(StringUtils.isNotNull(params.get("cc_position"))){
            String cc_position = params.get("cc_position").toString();
            //在什么节点抄送给抄送人
            req.setCcPosition(cc_position);
            startApprove.setCcPosition(cc_position);
        }
        List<OapiProcessinstanceCreateRequest.FormComponentValueVo> formComponentValueVoList = new ArrayList<>();
       if(StringUtils.isNotNull(params.get("form_component_values"))){
           JSONArray form_component_values = JSONObject.parseArray(JSON.toJSONString(params.get("form_component_values")));
           startApprove.setFormComponentValues(form_component_values.toJSONString());
           for(int i=0;i<form_component_values.size();i++) {
               OapiProcessinstanceCreateRequest.FormComponentValueVo formComponentValueVo = new OapiProcessinstanceCreateRequest.FormComponentValueVo();
               formComponentValueVoList.add(formComponentValueVo);
               formComponentValueVo.setName(form_component_values.getJSONObject(i).get("name").toString());
               formComponentValueVo.setValue(form_component_values.getJSONObject(i).get("value").toString());
           }
       }

        //设置审批人，会签、或签设置的审批人必须大于等于2个人
        List<OapiProcessinstanceCreateRequest.ProcessInstanceApproverVo> processInstanceApproverVoList = new ArrayList<>();
        if(StringUtils.isNotNull(params.get("approvers_v2"))){
            JSONArray approvers_v2 = JSONObject.parseArray(JSON.toJSONString(params.get("approvers_v2")));
            startApprove.setApproversV2(approvers_v2.toJSONString());
            for(int i=0;i<approvers_v2.size();i++) {
                OapiProcessinstanceCreateRequest.FormComponentValueVo formComponentValueVo = new OapiProcessinstanceCreateRequest.FormComponentValueVo();
                formComponentValueVoList.add(formComponentValueVo);

                OapiProcessinstanceCreateRequest.ProcessInstanceApproverVo processInstanceApproverVo = new OapiProcessinstanceCreateRequest.ProcessInstanceApproverVo();
                processInstanceApproverVoList.add(processInstanceApproverVo);
                processInstanceApproverVo.setTaskActionType(approvers_v2.getJSONObject(i).get("task_action_type").toString());
                processInstanceApproverVo.setUserIds(Arrays.asList(approvers_v2.getJSONObject(i).get("user_ids").toString().split(",")));
            }
            req.setApproversV2(processInstanceApproverVoList);
        }
       /* OapiProcessinstanceCreateRequest.ProcessInstanceApproverVo processInstanceApproverVo1 = new OapiProcessinstanceCreateRequest.ProcessInstanceApproverVo();
        processInstanceApproverVoList.add(processInstanceApproverVo1);
        processInstanceApproverVo1.setTaskActionType("OR");
        processInstanceApproverVo1.setUserIds(Arrays.asList("4525xxxxxxxx77041","2643xxxxxxx69379"));*/
        req.setFormComponentValues(formComponentValueVoList);
        OapiProcessinstanceCreateResponse rsp = null;
        try {
            rsp = client.execute(req, accessToken);
            bizLogger.info("[{}]发起审批成功; 返回内容：{}", startApprove.getOriginatorUserId(), JSON.toJSONString(rsp));
            //插入本地表
            ddStartApproveService.insertDdStartApprove(startApprove);
        } catch (ApiException e) {
            e.printStackTrace();
            bizLogger.error("发起审批实例失败", e.getErrMsg());
            return AjaxResult.error("发起审批实例失败");
        }
        return AjaxResult.success(JSON.parseObject(rsp.getBody()));
    }
}

