package com.ygsl.dingding.tools;

import com.dingtalk.api.DefaultDingTalkClient;
import com.dingtalk.api.DingTalkClient;
import com.dingtalk.api.request.OapiGettokenRequest;
import com.dingtalk.api.response.OapiGettokenResponse;
import com.taobao.api.ApiException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * 钉钉工具
 */
@Component
public class Ddtools {
    private static final Logger bizLogger = LoggerFactory.getLogger(Ddtools.class);
    public static String getAccessToken(String appKey, String appSecret) {
        DingTalkClient client = new DefaultDingTalkClient("https://oapi.dingtalk.com/gettoken");
        OapiGettokenRequest req = new OapiGettokenRequest();
        req.setAppkey(appKey);
        req.setAppsecret(appSecret);
        req.setHttpMethod("GET");
        OapiGettokenResponse rsp = null;
        try {
            rsp = client.execute(req);
            bizLogger.info("获取钉钉ACCESS_TOKEN:{}", rsp.getAccessToken());
        } catch (ApiException e) {
            e.printStackTrace();
            bizLogger.error("获取钉钉ACCESS_TOKEN失败;原因：{}", e.getErrMsg());
            return null;
        }
        return rsp.getAccessToken();
    }
}
