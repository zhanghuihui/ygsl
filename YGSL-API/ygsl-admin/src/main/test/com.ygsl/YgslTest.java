package com.ygsl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.dingtalk.api.DefaultDingTalkClient;
import com.dingtalk.api.DingTalkClient;
import com.dingtalk.api.request.OapiRoleListRequest;
import com.dingtalk.api.request.OapiV2DepartmentGetRequest;
import com.dingtalk.api.request.OapiV2DepartmentListsubidRequest;
import com.dingtalk.api.request.OapiV2UserListRequest;
import com.dingtalk.api.response.OapiRoleListResponse;
import com.dingtalk.api.response.OapiV2DepartmentGetResponse;
import com.dingtalk.api.response.OapiV2DepartmentListsubidResponse;
import com.dingtalk.api.response.OapiV2UserListResponse;
import com.taobao.api.ApiException;
import com.ygsl.common.utils.StringUtils;
import com.ygsl.dingding.domain.DdDept;
import com.ygsl.dingding.domain.DdRole;
import com.ygsl.dingding.domain.DdRoleGroup;
import com.ygsl.dingding.domain.DdUser;
import com.ygsl.dingding.mapper.DdDeptMapper;
import com.ygsl.dingding.mapper.DdRoleGroupMapper;
import com.ygsl.dingding.mapper.DdRoleMapper;
import com.ygsl.dingding.mapper.DdUserMapper;
import com.ygsl.dingding.tools.Ddtools;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * 测试类
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class YgslTest {
    private static final Logger bizLogger = LoggerFactory.getLogger(YgslTest.class);
    @Value("${taobao.dingding.appKey}")
    private String appKey;

    @Value("${taobao.dingding.appSecret}")
    private String appSecret;

    @Autowired(required = false)
    private DdRoleGroupMapper ddRoleGroupMapper;

    @Autowired(required = false)
    private DdRoleMapper ddRoleMapper;

    @Autowired(required = false)
    private DdDeptMapper ddDeptMapper;

    @Autowired(required = false)
    private DdUserMapper ddUserMapper;

    private List<Long> list = new ArrayList<>();
    /**
     * 同步DD所有部门
     */
    @org.junit.Test
    public void getDDDeptList() {
        String accessToken = Ddtools.getAccessToken(appKey, appSecret);
        //递归获取部门id列表
        recurDept(1L, accessToken);
        //获取部门详细信息
        if(!CollectionUtils.isEmpty(list)){
            try {
                for(Long deptId : list){
                    DingTalkClient client = new DefaultDingTalkClient("https://oapi.dingtalk.com/topapi/v2/department/get");
                    OapiV2DepartmentGetRequest req = new OapiV2DepartmentGetRequest();
                    req.setDeptId(deptId);
                    OapiV2DepartmentGetResponse rsp = client.execute(req, accessToken);
                    System.out.println(rsp.getBody());
                    JSONObject jsonObject = JSONObject.parseObject(rsp.getBody());
                    String errcode = jsonObject.get("errcode").toString();
                    if("0".equals(errcode)){
                        JSONObject result = JSONObject.parseObject(JSON.toJSONString( jsonObject.get("result")));
                        DdDept ddDept = new DdDept();
                        if(StringUtils.isNotNull(result.get("dept_id"))){
                            Long dept_id = Long.valueOf(result.get("dept_id").toString());
                            ddDept.setDeptId(dept_id);
                        }
                        if(StringUtils.isNotNull(result.get("parent_id"))){
                            Long parent_id = Long.valueOf(result.get("parent_id").toString());
                            ddDept.setParentId(parent_id);
                        }
                        if(StringUtils.isNotNull(result.get("name"))){
                            String name = result.get("name").toString();
                            ddDept.setName(name);
                        }
                        if(StringUtils.isNotNull(result.get("create_dept_group"))){
                            String create_dept_group = covertBooleanToChar((Boolean)result.get("create_dept_group"));
                            ddDept.setCreateDeptGroup(create_dept_group);
                        }
                        if(StringUtils.isNotNull(result.get("auto_add_user"))){
                            String auto_add_user = covertBooleanToChar((Boolean)result.get("auto_add_user"));
                            ddDept.setAutoAddUser(auto_add_user);
                        }
                        if(StringUtils.isNotNull(result.get("auto_approve_apply"))){
                            String auto_approve_apply = covertBooleanToChar((Boolean)result.get("auto_approve_apply"));
                            ddDept.setAutoApproveApply(auto_approve_apply);
                        }
                        if(StringUtils.isNotNull(result.get("from_union_org"))){
                            String from_union_org = covertBooleanToChar((Boolean)result.get("from_union_org"));
                            ddDept.setFromUnionOrg(from_union_org);
                        }
                        if(StringUtils.isNotNull(result.get("tags"))){
                            String tags = result.get("tags").toString();
                            ddDept.setTags(tags);
                        }
                        if(StringUtils.isNotNull(result.get("order"))){
                            Long order = Long.valueOf(result.get("order").toString());
                            ddDept.setDeptOrder(order);
                        }
                        if(StringUtils.isNotNull(result.get("dept_group_chat_id"))){
                            String dept_group_chat_id = result.get("dept_group_chat_id").toString();
                            ddDept.setDeptGroupChatId(dept_group_chat_id);
                        }
                        if(StringUtils.isNotNull(result.get("org_dept_owner"))){
                            String org_dept_owner = result.get("org_dept_owner").toString();
                            ddDept.setOrgDeptOwner(org_dept_owner);
                        }
                        if(StringUtils.isNotNull(result.get("dept_manager_userid_list"))){
                            JSONArray dept_manager_userid_list = (JSONArray)result.get("dept_manager_userid_list");
                            ddDept.setDeptManagerUseridList(StringUtils.join(dept_manager_userid_list, ","));
                        }
                        if(StringUtils.isNotNull(result.get("outer_dept"))){
                            String outer_dept = covertBooleanToChar((Boolean)result.get("outer_dept"));
                            ddDept.setOuterDept(outer_dept);
                        }
                        if(StringUtils.isNotNull(result.get("outer_permit_depts"))){
                            JSONArray outer_permit_depts = (JSONArray)result.get("outer_permit_depts");
                            ddDept.setOuterPermitDepts(StringUtils.join(outer_permit_depts, ","));
                        }
                        if(StringUtils.isNotNull(result.get("outer_permit_users"))){
                            JSONArray outer_permit_users = (JSONArray)result.get("outer_permit_users");
                            ddDept.setOuterPermitUsers(StringUtils.join(outer_permit_users, ","));
                        }
                        if(StringUtils.isNotNull(result.get("hide_dept"))){
                            String hide_dept = covertBooleanToChar((Boolean)result.get("hide_dept"));
                            ddDept.setHideDept(hide_dept);
                        }
                        if(StringUtils.isNotNull(result.get("user_permits"))){
                            JSONArray user_permits = (JSONArray)result.get("user_permits");
                            ddDept.setUserPermits(StringUtils.join(user_permits, ","));
                        }
                        if(StringUtils.isNotNull(result.get("dept_permits"))){
                            JSONArray dept_permits = (JSONArray)result.get("dept_permits");
                            ddDept.setDeptPermits(StringUtils.join(dept_permits, ","));
                        }
                        if(StringUtils.isNotNull(result.get("source_identifier"))){
                            String source_identifier = result.get("source_identifier").toString();
                            ddDept.setSourceIdentifier(source_identifier);
                        }
                        if(StringUtils.isNotNull(result.get("group_contain_sub_dept"))){
                            String group_contain_sub_dept = covertBooleanToChar((Boolean)result.get("group_contain_sub_dept"));
                            ddDept.setGroupContainSubDept(group_contain_sub_dept);
                        }
                        ddDeptMapper.insertDdDept(ddDept);
                    }
                }
            } catch (ApiException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
        bizLogger.info("[{}]发起审批成功", "1233");
    }
    private String covertBooleanToChar(Boolean bool){
        String flag = "0";
        if(bool){
            flag = "1";
        }
        return flag;
    }
    /**
     * 递归获取部门id
     * @return
     */
    private void recurDept(Long root, String accessToken){
        try {
            DingTalkClient client = new DefaultDingTalkClient("https://oapi.dingtalk.com/topapi/v2/department/listsubid");
            OapiV2DepartmentListsubidRequest req = new OapiV2DepartmentListsubidRequest();
            req.setDeptId(root);
            OapiV2DepartmentListsubidResponse rsp = client.execute(req, accessToken);
            System.out.println(rsp.getBody());
            JSONObject jsonObject = JSONObject.parseObject(rsp.getBody());
            String errcode = jsonObject.get("errcode").toString();
            JSONObject result = JSONObject.parseObject(JSON.toJSONString( jsonObject.get("result")));
            if("0".equals(errcode) && StringUtils.isNotNull(result.get("dept_id_list"))){
                JSONArray dept_id_lists = (JSONArray)result.get("dept_id_list");
                for(int j=0;j<dept_id_lists.size();j++) {
                    Long deptId = Long.valueOf(dept_id_lists.get(j).toString());
                    list.add(deptId);
                    recurDept(deptId, accessToken);
                }
            }
        } catch (ApiException e) {
            e.printStackTrace();
        }
    }
    /**
     * 同步DD所有人员
     */
    @org.junit.Test
    public void getDDUserList() {
        String accessToken = Ddtools.getAccessToken(appKey, appSecret);
        System.out.println("ACCESS_TOKEN:" + accessToken);
        try {
                DingTalkClient client = new DefaultDingTalkClient("https://oapi.dingtalk.com/topapi/v2/user/list");
                OapiV2UserListRequest req = new OapiV2UserListRequest();
                //根部门
                req.setDeptId(1L);
                req.setCursor(0L);
                //分页数 最大100
                req.setSize(100L);
                OapiV2UserListResponse rsp = client.execute(req, accessToken);
                System.out.println(rsp.getBody());
                JSONObject jsonObject = JSONObject.parseObject(rsp.getBody());
                String errcode = jsonObject.get("errcode").toString();
                if("0".equals(errcode)) {
                    JSONObject result = JSONObject.parseObject(JSON.toJSONString(jsonObject.get("result")));
                    JSONArray userList = JSONObject.parseArray(JSON.toJSONString( result.get("list")));
                    for(int j=0;j<userList.size();j++) {
                        DdUser ddUser = new DdUser();
                        JSONObject userJson = userList.getJSONObject(j);
                        if(StringUtils.isNotNull(userJson.get("userid"))){
                            String userid = userJson.get("userid").toString();
                            ddUser.setUserId(userid);
                        }
                        if(StringUtils.isNotNull(userJson.get("unionid"))){
                            String unionid = userJson.get("unionid").toString();
                            ddUser.setUnionId(unionid);
                        }
                        if(StringUtils.isNotNull(userJson.get("name"))){
                            String name = userJson.get("name").toString();
                            ddUser.setName(name);
                        }
                        if(StringUtils.isNotNull(userJson.get("avatar"))){
                            String avatar = userJson.get("avatar").toString();
                            ddUser.setAvatar(avatar);
                        }
                        if(StringUtils.isNotNull(userJson.get("state_code"))){
                            String state_code = userJson.get("state_code").toString();
                            ddUser.setStateCode(state_code);
                        }
                        if(StringUtils.isNotNull(userJson.get("mobile"))){
                            String mobile = userJson.get("mobile").toString();
                            ddUser.setMobile(mobile);
                        }
                        if(StringUtils.isNotNull(userJson.get("hide_mobile"))){
                            String hide_mobile = covertBooleanToChar((Boolean)userJson.get("hide_mobile"));
                            ddUser.setHideMobile(hide_mobile);
                        }
                        if(StringUtils.isNotNull(userJson.get("telephone"))){
                            String telephone = userJson.get("telephone").toString();
                            ddUser.setTelephone(telephone);
                        }
                        if(StringUtils.isNotNull(userJson.get("job_number"))){
                            String job_number = userJson.get("job_number").toString();
                            ddUser.setJobNumber(job_number);
                        }
                        if(StringUtils.isNotNull(userJson.get("title"))){
                            String title = userJson.get("title").toString();
                            ddUser.setTitle(title);
                        }
                        if(StringUtils.isNotNull(userJson.get("email"))){
                            String email = userJson.get("email").toString();
                            ddUser.setEmail(email);
                        }
                        if(StringUtils.isNotNull(userJson.get("org_email"))){
                            String org_email = userJson.get("org_email").toString();
                            ddUser.setOrgEmail(org_email);
                        }
                        if(StringUtils.isNotNull(userJson.get("work_place"))){
                            String work_place = userJson.get("work_place").toString();
                            ddUser.setWorkPlace(work_place);
                        }
                        if(StringUtils.isNotNull(userJson.get("remark"))){
                            String remark = userJson.get("remark").toString();
                            ddUser.setRemark(remark);
                        }
                        if(StringUtils.isNotNull(userJson.get("dept_id_list"))){
                            JSONArray dept_id_list = (JSONArray)result.get("dept_id_list");
                            ddUser.setDeptIdList(StringUtils.join(dept_id_list, ","));
                        }
                        if(StringUtils.isNotNull(userJson.get("dept_order"))){
                            String dept_order = userJson.get("dept_order").toString();
                            ddUser.setDeptOrder(dept_order);
                        }
                        if(StringUtils.isNotNull(userJson.get("extension"))){
                            String extension = userJson.get("extension").toString();
                            ddUser.setExtension(extension);
                        }
                        if(StringUtils.isNotNull(userJson.get("hired_date"))){
                            Long hired_date = Long.valueOf(userJson.get("hired_date").toString());
                            ddUser.setHiredDate(hired_date);
                        }
                        if(StringUtils.isNotNull(userJson.get("active"))){
                            String active = covertBooleanToChar((Boolean)userJson.get("active"));
                            ddUser.setActive(active);
                        }
                        if(StringUtils.isNotNull(userJson.get("admin"))){
                            String admin = covertBooleanToChar((Boolean)userJson.get("admin"));
                            ddUser.setAdmin(admin);
                        }
                        if(StringUtils.isNotNull(userJson.get("boss"))){
                            String boss = covertBooleanToChar((Boolean)userJson.get("boss"));
                            ddUser.setBoss(boss);
                        }
                        if(StringUtils.isNotNull(userJson.get("leader"))){
                            String leader = covertBooleanToChar((Boolean)userJson.get("leader"));
                            ddUser.setLeader(leader);
                        }
                        if(StringUtils.isNotNull(userJson.get("exclusive_account"))){
                            String exclusive_account = covertBooleanToChar((Boolean)userJson.get("exclusive_account"));
                            ddUser.setExclusiveAccount(exclusive_account);
                        }
                        if(StringUtils.isNotNull(userJson.get("login_id"))){
                            String login_id = userJson.get("login_id").toString();
                            ddUser.setLoginId(login_id);
                        }
                        if(StringUtils.isNotNull(userJson.get("exclusive_account_type"))){
                            String exclusive_account_type = userJson.get("exclusive_account_type").toString();
                            ddUser.setExclusiveAccountType(exclusive_account_type);
                        }
                        if(StringUtils.isNotNull(userJson.get("nickname"))){
                            String nickname = userJson.get("nickname").toString();
                            ddUser.setNickName(nickname);
                        }
                        ddUserMapper.insertDdUser(ddUser);
                    }
                }
            } catch (ApiException e) {
                e.printStackTrace();
            }
    }

    /**
     * 同步DD所有角色
     */
    @org.junit.Test
    public void getDDRoleList() {
        String accessToken = Ddtools.getAccessToken(appKey, appSecret);
        System.out.println("ACCESS_TOKEN:" + accessToken);
        try {
            DingTalkClient client = new DefaultDingTalkClient("https://oapi.dingtalk.com/topapi/role/list");
            OapiRoleListRequest req = new OapiRoleListRequest();
            OapiRoleListResponse rsp = client.execute(req, accessToken);
            System.out.println(rsp.getBody());
            JSONObject jsonObject = JSONObject.parseObject(rsp.getBody());
            JSONObject result = JSONObject.parseObject(JSON.toJSONString( jsonObject.get("result")));
            JSONArray roleGroups = JSONObject.parseArray(JSON.toJSONString( result.get("list")));
            if(!CollectionUtils.isEmpty(roleGroups)){
                for(int i=0;i<roleGroups.size();i++) {
                    DdRoleGroup ddRoleGroup = new DdRoleGroup();
                    ddRoleGroup.setGroupId(Long.valueOf(roleGroups.getJSONObject(i).get("groupId").toString()));
                    ddRoleGroup.setName((roleGroups.getJSONObject(i).get("name").toString()));
                    ddRoleGroupMapper.insertDdRoleGroup(ddRoleGroup);
                    JSONArray roles = JSONObject.parseArray(JSON.toJSONString(roleGroups.getJSONObject(i).get("roles")));
                    for(int j=0;j<roles.size();j++) {
                        DdRole ddRole = new DdRole();
                        ddRole.setGroupId(ddRoleGroup.getGroupId());
                        ddRole.setRoleId(Long.valueOf(roles.getJSONObject(j).get("id").toString()));
                        ddRole.setName((roles.getJSONObject(j).get("name").toString()));
                        ddRoleMapper.insertDdRole(ddRole);
                    }
                }
            }
        } catch (ApiException e) {
            e.printStackTrace();
        }
    }
  /*  @org.junit.Test
    public void listDept(){
       String accessToken = Ddtools.getAccessToken(appKey, appSecret);
        try {
            DingTalkClient client = new DefaultDingTalkClient("https://oapi.dingtalk.com/topapi/v2/department/listsub");
            OapiV2DepartmentListsubRequest req = new OapiV2DepartmentListsubRequest();
            //部门id
            req.setDeptId(1L);
            OapiV2DepartmentListsubResponse rsp = client.execute(req, accessToken);
            System.out.println(rsp.getBody());
        } catch (ApiException e) {
            e.printStackTrace();
        }
    }*/
}
