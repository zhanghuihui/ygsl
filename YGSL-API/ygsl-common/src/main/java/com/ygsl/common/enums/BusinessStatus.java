package com.ygsl.common.enums;

/**
 * 操作状态
 * 
 * @author ygsl
 *
 */
public enum BusinessStatus
{
    /**
     * 成功
     */
    SUCCESS,

    /**
     * 失败
     */
    FAIL,
}
